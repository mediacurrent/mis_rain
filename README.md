# Mediacurrent Rain Install Profile #

__IMPORTANT__: The Rain profile has a new home, please visit https://bitbucket.org/mediacurrent/rain/.

The Mediacurrent Rain install profile adds out-of-the-box editorial, administrative and media enhancements to the typical Drupal 8 installation.
The profile creates media types (video, image), but all other content types are optional. Rain can be installed as a stand-alone profile or can be used as a base profile.

### Key features ###

* Content moderation (core)
* Revisions (core)
* Media management (media_entity, entity_browser)
* Wysiwyg media + dialog (entity_embed)
* Focus crop (focal_point)
* Wysiwyg Linkit (linkit)
* Paragraphs (paragraphs)
* Content scheduling (scheduler)
* Admin menu (admin_toolbar)
* Path aliases (pathauto)
* XML Sitemap config (simple_sitemap)
* Metatag configuration (metatag)
* Analytics (google_analytics)
* Taxonomy access fix for editors (taxonomy_access_fix)

### Optional features
* [rain_blocks](https://bitbucket.org/mediacurrent/mis_rain/src/develop/modules/rain_blocks/) - Provides common block types. 
* [rain_content](https://bitbucket.org/mediacurrent/mis_rain/src/develop/modules/rain_content/) - Provides common content types. 
* [rain_paragraphs](https://bitbucket.org/mediacurrent/mis_rain/src/develop/modules/rain_paragraphs/) - Provides common paragraphs
* rain_search - Enables Search API search index and page

### Installation ###

* The intended usage of this project is to be pulled into a full [Drupal composer-based project](https://bitbucket.org/mediacurrent/drupal-project/).
* See [MIS Rain Vagrant](https://bitbucket.org/mediacurrent/mis_rain_vagrant/src) project for example usage. Note that this project uses Rain as a base profile.
* To install as a stand-alone install, use the command: ```drush si -y rain```
* See [Drupal Sub-profiles thread](https://www.drupal.org/node/1356276) for details around using an install profile as a "subprofile."
* This profile can also be forked easily by renaming files and replacing project name with custom project name.

### Usage ###

The Rain install profile is primarily used to help with Drupal core/contrib dependency management and provide configuration that can be easily modified. 
It is not necessarily recommended for applications using the profile to import downstream configuration updates. Configuration provided is intended as a 
"starter" when the install profile is run for the very first time. Once configuration has been committed to git, downstream updates will not effect
the Drupal 8 application (by design). Downstream Composer updates however can be extremely beneficial as they reduce the maintenance burden for application owners to manage
dozens of dependencies provided by this install profile. Pulling down these updates is recommended.

### Making configuration updates to this project

* One aspect of making updates to this project is that its important to remove uuids/hashes from configuration. The base script below will do this for you.
```
#!/bin/bash
FILES=config/install/*
for f in $FILES
do
  echo "Processing $f file..."
  sed -i '' '/^uuid:/d' $f
  sed -i '' '/^\_core:/d' $f
  sed -i '' '/^  default_config_hash:/d' $f
done
```