<?php

namespace Drupal\rain_calendar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

define('DEMO_30_DAYS_UNIX', 2592000000);

/**
 * Controller routines for Event calendar routes.
 */
class EventCalendar extends ControllerBase {

  /**
   * Renders the calendar page.
   */
  public function calendar() {
    $build = [];
    $build['content'] = [
      '#theme' => 'event_calendar',
    ];
    return $build;
  }

  /**
   * Renders events json for fullCalendar.
   */
  public function json() {
    global $base_url;
    header('Content-Type: application/json');

    // Get 100 events starting 30 days before today.
    $result = db_query('SELECT n.nid FROM {node} n
      JOIN {node_field_data} nd ON nd.vid = n.vid
      JOIN {node__field_dates} d ON n.nid = d.entity_id AND d.bundle = :bundle
      WHERE d.delta = 0 AND n.type = :type AND d.field_dates_value >= :date
      AND nd.status = 1 LIMIT 100',
      [
        ':type' => 'event',
        ':date' => REQUEST_TIME - DEMO_30_DAYS_UNIX,
        ':bundle' => 'event',
      ]
    );

    $json = [];

    while ($row = $result->fetchObject()) {
      $node = node_load($row->nid);
      $values = $node->field_dates->getValue();

      // Convert to local time from UTC.
      $start = format_date(strtotime($values[0]['value'] . ' UTC'), '', 'Y-m-d\TH:i:s');
      $end = !empty($values[0]['end_value']) ? format_date(strtotime($values[0]['end_value'] . ' UTC'), '', 'Y-m-d\TH:i:s') : $start;

      $json[] = [
        'title' => $node->getTitle(),
        'id' => $node->id(),
        'url' => $node->url(),
        'start' => $start,
        'end' => $end,
      ];
    }

    return new JsonResponse($json);
  }

}
